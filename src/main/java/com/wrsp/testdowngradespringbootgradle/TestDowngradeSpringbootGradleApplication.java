package com.wrsp.testdowngradespringbootgradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestDowngradeSpringbootGradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestDowngradeSpringbootGradleApplication.class, args);
	}

}
